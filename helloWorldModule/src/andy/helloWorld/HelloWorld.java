package andy.helloWorld;

import andy.helloWorld.HelloInterface;
import andy.helloWorld.HelloWorldNoAccess;

public class HelloWorld implements HelloInterface {

    HelloWorldNoAccess hwna = new HelloWorldNoAccess();
    String fromHwna = hwna.giveString();

    public void print(){
        System.out.println("HelloWorld");
    }

    public void printStringFromNoAccess(){
        System.out.println(fromHwna + " printed from HelloWorld.class");
    }

    @Override
    public void sayHello() {
        System.out.println("HelloInterface implemented in HelloWorld.class created as service");
    }
}
