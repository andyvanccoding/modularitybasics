package andy.helloWorld;

class HelloWorldNoAccess {

    public void print(){
        System.out.println("No access!");
    }

    public String giveString(){
        return "Given string from no access";
    }
}
