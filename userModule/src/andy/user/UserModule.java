package andy.user;

import andy.staticWorld.StaticWorld;
import andy.helloOtherWorld.HelloOtherWorld;
import andy.helloWorld.HelloInterface;
import andy.helloWorld.HelloWorld;
import andy.transitiveWorld.HelloTransitiveWorld;

import java.util.ServiceLoader;
import java.util.Set;
import java.util.stream.Collectors;

public class UserModule {
    public static void main(String[] args) {
        HelloWorld hw = new HelloWorld();
        hw.print();
        System.out.println("\nThis shows possibility to take from a package private class");
        hw.printStringFromNoAccess();

//        not accessible due to package private
//        HelloWorldNoAccess test = new HelloWorldNoAccess();
//        test.print();

        Iterable<HelloInterface> services = ServiceLoader.load(HelloInterface.class);

        System.out.println("\nThis will use one implementation of the interface");
        HelloInterface service = services.iterator().next();
        service.sayHello();

        System.out.println("\nThis will go over all the implementations of the interface");
        services.forEach(HelloInterface::sayHello);

        System.out.println("\nThis will print all the classes implementing the interface");
        ServiceLoader<HelloInterface> loader = ServiceLoader.load(HelloInterface.class);
        Set<HelloInterface> printFactories = loader
                .stream()
                .map(ServiceLoader.Provider::get)
                .collect(Collectors.toSet());


        printFactories.forEach(System.out::println);

        System.out.println("\nOther stuff from down here!");
        HelloOtherWorld how = new HelloOtherWorld();
        how.print();

        StaticWorld sw = new StaticWorld();

        String staticString = sw.giveStaticString();
        System.out.println(staticString);

        System.out.println("This was accessible due to transitive requirement of the" +
                "otherWorld.class");
        HelloTransitiveWorld htw = new HelloTransitiveWorld();
        htw.print();


    }
}
